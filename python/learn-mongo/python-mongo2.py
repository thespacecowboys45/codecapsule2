from pymongo import MongoClient
# pprint library is used to make the output look more pretty
from pprint import pprint
# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string

mongoUrl = "mongodb://ld10873.homedepot.com:27017/extrahealth"
client = MongoClient(mongoUrl)


#db=client.admin


db=client.extrahealth


# Issue the serverStatus command and print the results
#serverStatusResult=db.command("serverStatus")
#pprint(serverStatusResult)

appliance = db.appliances.find_one({'host':'atl-dev-eh01.homedepot.com'})

print(appliance)