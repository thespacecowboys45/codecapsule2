#!/usr/bin/env bash
#
# How to iterate over a file in bash
#
# NOTE: The last line in the input file must have a return character on it for this to work completely.
#
####
#FILENAME="example-input.txt"
FILENAME=$1

if [ x${FILENAME} == "x" ]
then
    echo "No input file specified."
    exit
fi

while read p; do
    echo "${p}"
done <${FILENAME}
