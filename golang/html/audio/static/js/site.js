var roarSound;


$(document).ready(function() {

    //alert("hello world");

    loadSounds();
    loadSounds2();
    playRoar();
});

function loadSounds2() {

    if (roarSound == undefined) {
        roarSound = new Howl({
            src: ['/static/audio/Godzilla_Roar-Marc-1912765428.mp3']
        });
    }
}

function loadSounds() {
    var audioElement = document.createElement('audio');
    //audioElement.setAttribute('src', 'http://www.soundjay.com/misc/sounds/bell-ringing-01.mp3');
    audioElement.setAttribute('src', '/static/audio/Godzilla_Roar-Marc-1912765428.mp3');

    audioElement.addEventListener('ended', function() {
        this.play();
    }, false);

    audioElement.addEventListener("canplay",function(){
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color","green");
    });

    audioElement.addEventListener("timeupdate",function(){
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play').click(function() {
        audioElement.play();
        $("#status").text("Status: Playing");
    });

    $('#pause').click(function() {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function() {
        audioElement.currentTime = 0;
    });
}

function playRoar() {
    roarSound.play();
}