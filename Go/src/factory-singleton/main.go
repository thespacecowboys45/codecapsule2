package main

/////////
//
// Docs:
// Singleton pattern: https://medium.com/golang-issue/how-singleton-pattern-works-with-golang-2fdd61cd5a7f
// Factory pattern: https://www.sohamkamani.com/golang/2018-06-20-golang-factory-patterns/
//
// Design:
// app Config, master, and worker
//
// master and worker talk through two channels, one for each to "hear" commands
// appConfig stores references to master
// master is in charge of obtaining and managing worker and worker resources
//
// on boot - create appConfig
//           create a master
//
//           master is responsible for creating workers
//
//
/////////
import (
	"fmt"
	"time"
)

type App struct {
	cfg *AppConfig
}

type AppConfig struct {
	id     int
	master *Master
}

func GetAppConfig(id int) *AppConfig {

	// Singleton pattern
	if appConfig == nil {
		appConfig = &AppConfig{
			id: id,
		}
	}

	return appConfig
}

func (ag *AppConfig) printAppConfig() {
	fmt.Printf("AppConfig id: %d\n", ag.id)
}

// declare a global (one way)
var g int
var app *App
var appConfig *AppConfig

func goToWork() {
	fmt.Printf("[main.go][goToWork][entry]")

	// get reference to app config
	ac := GetAppConfig(0)
	ac.master.getWorker()
	ac.master.startWork()

	// Wait
	time.Sleep(5000 * time.Millisecond)

	ac.master.stopWork()
}

func main() {
	// examples of using global variable
	wg = 55 // Yeah, not declared in here.  Did you look for it yet?
	g = 4

	// create an app
	app = &App{}

	// instantiate app configuration
	ac := GetAppConfig(1)
	ac.printAppConfig()
	app.cfg = ac

	// can access global structure
	ac.id = 3
	ac.printAppConfig()

	// create a master
	m := NewMaster(1)
	// set the master
	ac.master = m

	goToWork()

	// wait forever
	select {}
}
