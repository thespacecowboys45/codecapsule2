package main

import (
	"fmt"
)

type Master struct {
	Id     int
	Chan   chan int
	Worker *Worker
}

func NewMaster(id int) *Master {
	return &Master{
		Id:   id,
		Chan: make(chan int),
	}
}

func (m *Master) getWorker() *Worker {
	fmt.Printf("[master.go][getWorker][entry]\n")

	if m.Worker == nil {
		// create a new worker and give them our channel to talk back on
		w := NewWorker(1, m.Chan)
		m.Worker = w
	}

	fmt.Printf("[master.go][getWorker][exit]\n")
	return m.Worker
}

func (m *Master) startWork() {
	// master works by handing work to workers
	fmt.Printf("[master.go][startWork][entry]\n")

	// get reference to the AppConfig
	// For fun.  Not used.  As example:
	// we can get a database connection from AppConfig.
	ac := GetAppConfig(1)
	ac.printAppConfig()

	// make sure we have a worker
	w := m.getWorker()
	// tell worker to go to work
	w.startWork()
	fmt.Printf("[master.go][startWork][exit]\n")
}

func (m *Master) stopWork() {
	fmt.Printf("[master.go][stopWork][entry]\n")
	m.Worker.stopWork()
	fmt.Printf("[master.go][stopWork][exit]\n")
}

func (m *Master) handleMsgFromWorker() {
	fmt.Printf("[master.go][handleMsgFromWorker][entry]\n")
	// @TODO - handle messages from children
	fmt.Printf("[master.go][handleMsgFromWorker][exit]\n")
}

func (m *Master) printGlobal() {
	fmt.Printf("[master.go][Global: %d]\n", g)
}
