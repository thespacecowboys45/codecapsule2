#!/usr/bin/python

# One easy way to create a block comment is to make a text variable with your documentation
DOCUMENTATION = '''
---
A program which calls a function
'''

def do_anything():
    print("[ do_anything () ]")

# a function which takes a parameter
def do_something_specific(the_task):
    print("[ do_something_specific like: " + the_task + " ]")


def main():
    print("This is main")
    do_anything()
    do_something_specific("Go to the grocery store")

main()
