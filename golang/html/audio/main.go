package main

import(
    "fmt"
    "log"
    "net/http"
    dlog "bitbucket.org/thespacecowboys45/dlogger"
    "gopkg.in/alecthomas/kingpin.v2"
)

var (
    logLevel = kingpin.Flag("loglevel", "Set the log output level.  Default 'debug'.").Default("debug").Envar("LOG_LEVEL").String()
)

func configureDlogger() {
    dlog.SetLevel(*logLevel)
}

func printFlags() {
    dlog.Always("logLevel:%s", *logLevel)
}

func mainHandler(w http.ResponseWriter, h *http.Request) {
    fmt.Printf("Got request\n")
}

func main() {
    fmt.Printf("Hello world")
    kingpin.Parse()
    configureDlogger()
    printFlags()

    http.HandleFunc("/", mainHandler)


    fs := http.FileServer(http.Dir("static"))
    http.Handle("/static/", http.StripPrefix("/static/", fs))

    listenPort := ":8002"

    log.Println("Listening on port ", listenPort, " ...")
    //http.ListenAndServe(":", nil)

    log.Fatal(http.ListenAndServe(listenPort, nil))

}