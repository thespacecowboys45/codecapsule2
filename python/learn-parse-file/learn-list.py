#!/usr/bin/python

# One easy way to create a block comment is to make a text variable with your documentation
DOCUMENTATION = '''
---
Create a list in python.  Iterate over the list and print out the values to the screen
'''

# A simple array
print("SIMPLE ARRAY")
cars = ["Ford", "Volvo", "BMW"]
# open the file for reading

for car in cars:
    print(car)



# A two-dimensional array
print("TWO DIMENSIONAL ARRAY")
a = [[2, 4, 6, 8, 10], [3, 6, 9, 12, 15], [4, 8, 12, 16, 20]]

for record in a:
    print(record)


# An associative array (aka. a dictionary)
print("ASSOCIATIVE ARRAY")
city_population = {"New York City":8550405, "Los Angeles":3971883, "Toronto":2731571, "Chicago":2720546, "Houston":2296224, "Montreal":1704694, "Calgary":1239220, "Vancouver":631486, "Boston":667137}
for city in city_population:
    # typecast to a string so we can print it out inline
    population = str(city_population[city])
    print("city=" + city + " population=" + population)


json_data = [{"k1":"v1", "k2":"v2"}, {"k3":"v3", "k4":"v4"}]
for list_item in json_data:
    print("list_item="+str(list_item))
    for item in list_item:
        print("key="+item + " value:" + list_item[item])