package main

import (
	"io"
	"log"
	"os"
)

/**
 * function will write to stdout and also a file
 */
func logMsg(msg string) {
	log.Printf(msg)
}

func check(err error) {
	if err != nil {
			log.Fatal(err)
	}
}

func setupLogFile() {
	logFile, err := os.OpenFile("thelog.log", os.O_CREATE | os.O_APPEND | os.O_RDWR, 0666)
	check(err)

	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	//defer logFile.Close()
}

func main() {
	setupLogFile()

	logMsg("[main]\n")
}