package main

import (
	"fmt"
	"time"
)

type Worker struct {
	id           int
	Chan         chan int
	masterChan   chan int
	PeriodTicker *time.Ticker
}

func NewWorker(id int, c chan int) *Worker {
	fmt.Printf("[worker.go][NewWorker][entry]\n")

	wChan := make(chan int)
	// I like declaring explicitly rather than inside the return statement
	t := time.NewTicker(1000 * time.Millisecond)
	return &Worker{
		id:           id,
		Chan:         wChan,
		masterChan:   c,
		PeriodTicker: t,
	}
}

// Can declare global variables anywhere in the same package
var wg int

func (w *Worker) startWork() {
	fmt.Printf("[%d][worker.go][startWork][entry]\n", w.id)
	//w.talkToMaster()
	go func() {
		for {
			select {
			case <-w.Chan:
				fmt.Printf("[%d][worker.go][startWork][got message from pipe]\n", w.id)

				// Clean up and exit
				w.cleanUp()
				fmt.Printf("[worker.go][startWork][exit on command]\n")
				return
			case t := <-w.PeriodTicker.C:
				fmt.Printf("[%d][worker.go][tick at %v]\n", w.id, t)
			}
		}
	}()
	fmt.Printf("[worker.go][startWork][exit]\n")
}

func (w *Worker) stopWork() {
	fmt.Printf("[worker.go][stopWork][entry]\n")
	// talk to self through the pipe
	w.Chan <- 1
	fmt.Printf("[worker.go][stopWork][exit]\n")
}

func (w *Worker) cleanUp() {
	fmt.Printf("[worker.go][cleanUp][entry]\n")
	// Talk to master through pipe to let him know we are done done done done
	w.masterChan <- 1
	fmt.Printf("[worker.go][cleanUp][exit]\n")
}

func (w *Worker) printGlobal() {
	fmt.Printf("[worker.go][Global: %d]\n", g)

	fmt.Printf("[worker.go][worker Global: %d]\n", wg)
}

func (w *Worker) talkToMaster() {
	fmt.Printf("[worker.go][talkToMaster][entry]\n")
	ac := GetAppConfig(0)
	ac.master.handleMsgFromWorker()
}
