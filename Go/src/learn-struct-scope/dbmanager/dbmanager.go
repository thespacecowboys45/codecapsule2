package dbmanager

// I think I'm going to need to reference these another time:
//
// https://medium.com/golang-issue/how-singleton-pattern-works-with-golang-2fdd61cd5a7f
// https://stackoverflow.com/questions/41257847/how-to-create-singleton-db-class-in-golang
//
// What I want to create is a database class which is the entry point for all calls
// to the database.  It also holds a single connection to the database which can
// stay open and be re-used, and be used by other goroutines
//
/////

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	sp "../secondpkg"
	//"gopkg.in/mgo.v2/bson"
	"strings"
	"time"
)

// Note - these are global variable which are local to this package
var (
	host                  = ""
	db                    = ""
	username              = ""
	password              = ""
	replicaset            = ""
	appliances_collection = ""
)

type dbManager struct {
	DbSession *mgo.Session
	S2        mgo.Session
}

func NewDbManager() dbManager {

	//s := Connect()

	return dbManager{}
}

func (dm dbManager) GetSecondStruct() *sp.SecondStruct {
	return &sp.SecondStruct{
		Var1: "querty",
		Var2: "uiop",
	}
}

//////
// NOTE: This is THE KEY!
// you must take in a "pointer" to the structure, notice the "*" character
// in front of the type (dm *dbManager) in order to modify the actual structure
// instead of a copy of the structure, and to set variables inside the
// desired copy of the data structure
//////
func (dm *dbManager) DoIt() {
	fmt.Printf("doIt()\n")

	s := dm.Connect()

	fmt.Printf("doIt() - s: %v\n", s)

	// I believe this is setting global variables in the package
	// rather than setting anything in a specific instantiation of this class
	dm.DbSession = s
	dm.S2 = *s
}

func (dm dbManager) Connect() *mgo.Session {
	fmt.Printf("dbManager::Connect()\n")

	dbHosts := strings.Split(host, ",")

	/* From: https://gist.github.com/345161974/4f2048f90584a64891cf07997bfd9e23 */
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:          dbHosts,
		Timeout:        15 * time.Second,
		Database:       db,
		Username:       username,
		Password:       password,
		ReplicaSetName: replicaset,
	}

	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		fmt.Printf("Error creating DB session: %s\n", err)
	} else {
		fmt.Printf("Connection estsablished: &session: %v\nsession: %v\n*session: %v\n", &session, session, *session)
	}

	// I CAN do this but only if the declaration of the type is a pointer
	// meaning the function declaration would have to look like
	//
	// func (dm *dbManager) Connect() *mgo.Session {
	//
	// instead of
	//
	// func (dm dbManager) Connect() *mgo.Session {
	//
	// Notice the splat "*" above instead of it missing like it is currently
	//

	//dm.DbSession = session
	//dm.s2 = *session

	return session
}

func (dm dbManager) MarkInProgress(ehop string) {
	fmt.Printf("dm.dbManager.MarkInProgress ehop=%s\n", ehop)

	if dm.DbSession != nil {
		c := dm.DbSession.DB(db).C(appliances_collection)

		currentTime := time.Now()

		err := c.Update(bson.M{"host": ehop}, bson.M{"$set": bson.M{"ehealth_work_status": "in_progress",
			"ehealth_work_timestamp": currentTime.Format("2006-01-02 15:04:05")}})
		if err != nil {
			fmt.Printf("dm.DbManager() -- Failed to set %s as active: %s\n", ehop, err)
		}
	} else {
		fmt.Printf("dm.DBManager.MarkInProgress() - no database session open\n")
	}
}

func Thereyougo() {
	fmt.Printf("Thereyougo() : %s\n", host)
}
