package main

import (
	dbm "./dbmanager"
	tp "./testpkg"
	"fmt"
)

type theStruct1 struct {
	var1 string
}

type theStruct2 struct {
	var1 string
}

type theStruct3 struct {
	s1 *theStruct1
	s2 theStruct2
}

func New1(val string) *theStruct1 {
	return &theStruct1{
		var1: val,
	}
}

func New2(val string) theStruct2 {
	return theStruct2{
		var1: val,
	}
}

func New3a() {

}

func New3(val1 string, val2 string) theStruct3 {

	s1 := New1(val1)
	s2 := New2(val2)

	return theStruct3{
		s1: s1,
		s2: s2,
	}
}

// Note - these are global variable which are local to this package
var (
	host       = ""
	db         = ""
	username   = ""
	password   = ""
	replicaset = ""
)

func printAddr2(s *tp.Astruct) {
	fmt.Printf("printAddr2: %v\n", &s)
}

func main() {

	s1 := New1("foo")
	fmt.Printf("s1=%v val=%s\n", s1, s1.var1)

	s2 := New2("bar")
	fmt.Printf("s2=%v val=%s\n", s2, s2.var1)

	s3 := New3("biz", "baz")
	fmt.Printf("s3=%v v1=%s, v2=%s\n", s3, s3.s1.var1, s3.s2.var1)
	fmt.Printf("s3.s1=%v s3.s2=%v\n", s3.s1, s3.s2)

	//Connect()

	dbm.Thereyougo()

	m := dbm.NewDbManager()
	//s := m.Connect()

	//fmt.Printf("s=%v\n", s)
	//m.DbSession = s

	//dbm.Connect()
	//m.DoIt()

	fmt.Printf("m: %v\n&m: %v\n", m, &m)
	fmt.Printf("m.dbSession: %v\n", m.DbSession)
	fmt.Printf("m.s2: %v\n", m.S2)

	m.MarkInProgress("extrahop-rx.st0448.homedepot.com")

	secondstruct := m.GetSecondStruct()
	fmt.Printf("secondstruct: %v\n", secondstruct)

	fmt.Printf("\n-------------------------\n")

	a := tp.Getstruct("biz", "baz")
	fmt.Printf("1111 a: %v\n&a: %v\n*a: %v\n", a, &a, *a)
	printAddr2(a)
	a.ShowMe()

	a.Setvals1("aaa", "bbb")
	fmt.Printf("2222 a: %v\n&a: %v\n*a: %v\n", a, &a, *a)

	a.Setvals2(a, "ccc", "ddd")
	fmt.Printf("3333 a: %v\n&a: %v\n*a: %v\n", a, &a, *a)

	tp.Setvals3(a, "eee", "fff")
	fmt.Printf("4444 a: %v\n&a: %v\n*a: %v\n", a, &a, *a)

	a.Setvals4("ggg", "hhh")
	fmt.Printf("5555 a: %v\n&a: %v\n*a: %v\n", a, &a, *a)

	fmt.Printf("\n.... Exiting ....\n")
}
