package main

import(
	"fmt"
)

type theStruct1 struct{
	var1 string
}

type theStruct2 struct{
	var1 string
}

type theStruct3 struct{
	s1 *theStruct1
	s2 theStruct2
}

func New1(val string) *theStruct1 {
	return &theStruct1{
		var1: val,
	}
}

func New2(val string) theStruct2 {
	return theStruct2{
		var1: val,
	}
}

func New3a() {

}

func New3(val1 string, val2 string) theStruct3 {

	s1 := New1(val1)
	s2 := New2(val2)

	return theStruct3{
		s1: s1,
		s2: s2,
	}
}



func main() {

	s1 := New1("foo")
	fmt.Printf("s1=%v val=%s\n", s1, s1.var1)

	s2 := New2("bar")
	fmt.Printf("s2=%v val=%s\n", s2, s2.var1)

	s3 := New3("biz", "baz")
	fmt.Printf("s3=%v v1=%s, v2=%s\n", s3, s3.s1.var1, s3.s2.var1)
	fmt.Printf("s3.s1=%v s3.s2=%v\n", s3.s1, s3.s2)
}