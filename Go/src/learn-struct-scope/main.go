package main

import (
	dlog "bitbucket.org/thespacecowboys45/dlogger"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"time"
)

type DbManager struct {
	//host                 []string
	host                  string
	db                    string
	username              string
	password              string
	replicaset            string
	appliances_collection string
	dbSession             *mgo.Session
}

func (dm DbManager) EstablishPersistentConnection() *mgo.Session {

	dlog.Always("DbManager::EstablishPersistentConnection() - ")

	dbHosts := strings.Split(dm.host, ",")

	/* From: https://gist.github.com/345161974/4f2048f90584a64891cf07997bfd9e23 */
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:          dbHosts,
		Timeout:        15 * time.Second,
		Database:       dm.db,
		Username:       dm.username,
		Password:       dm.password,
		ReplicaSetName: dm.replicaset,
	}

	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		fmt.Printf("Error creating DB session: %s\n", err)
	}
	fmt.Printf("Persistent connection estsablished: %v\n", session)

	// Don't do this here - it's not going to remain in scope
	// Instead return the session and use it outside the scope of this function
	//
	//dm.dbSession = session

	// Don't close the session when this function exits
	//defer session.Close()

	dlog.Always("Session Info: %v\n", session)

	return session

}

func NewDbManager(host string, db string, username string, password string, replicaset string, appliances_collection string) DbManager {

	// @TODO - to support dev database (authenticated)
	// Create list of hosts from host parameter.  Split on a comma
	//dbHosts := []string{"lnc66e7.homedepot.com:27017","lnc66e8.homedepot.com:27017","lnc66e9.homedepot.com:27017"}

	return DbManager{
		host:                  host,
		db:                    db,
		username:              username,
		password:              password,
		replicaset:            replicaset,
		appliances_collection: appliances_collection,
		dbSession:             nil,
	}
}

func (dm DbManager) MarkInProgress(ehop string) {
	dlog.Always("DbManager::MarkInProgress() - %s - session: %v", ehop, dm.dbSession)

	if dm.dbSession != nil {
		c := dm.dbSession.DB(dm.db).C(dm.appliances_collection)

		err := c.Update(bson.M{"host": ehop}, bson.M{"$set": bson.M{"ehealth_work_status": "in_progress"}})
		if err != nil {
			dlog.Always("dm.DbManager() -- Failed to set %s as active: %s", ehop, err)
		}
	} else {
		dlog.Always("DbManager.MarkInProgress() - no database session is open")
	}

}

func main() {
	fmt.Printf("main()\n")

	host := ""
	db := ""
	username := ""
	password := ""
	replicaset := ""
	collection := ""

	dm := NewDbManager(host, db, username, password, replicaset, collection)

	s := dm.EstablishPersistentConnection()
	// Need to set this here.  I think this is a scope thing and you cannot set the variable inside the function above
	dm.dbSession = s

	dlog.Always("Session NOW: %v\n", dm.dbSession)

	fmt.Printf("dm=%v\n", dm)

	dm.MarkInProgress("extrahop-rx.st0121.homedepot.com")
}
