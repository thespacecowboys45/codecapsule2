#!/usr/bin/python

# One easy way to create a block comment is to make a text variable with your documentation
DOCUMENTATION = '''
---
A program which:
 - opens a .csv file
 - iterates over the contents of the file
 - counts the number of unique entries in the second column based on the first column
 - outputs the results to the screen
'''

final_data = {}


def sort_data(key, value):
    print("[ sort_data() key=" + key + ", value=" + value + " ]")

    if value in final_data:
        current_count = final_data[value]
    else:
        current_count = 0

    # increment the counter
    current_count = current_count + 1

    print("[ current_count = " + str(current_count) + " ]")
    final_data[value] = current_count

def print_results():
    for k in final_data:
        print("[ final_data = " + k + " count = " + str(final_data[k]) + "]")

def main():
    print("This is main")

    # the name of our file to read
    filename = "data.csv"

    # open the file for reading
    f = open(filename, "r")

    # iterate over each line in the file
    for line in f:
        # strip off the return character from the end of the line
        line = line.strip("\r\n")

        # split the data based on a comma
        parts = line.split(",")
        sort_data(parts[0], parts[1])

    print_results()

main()
