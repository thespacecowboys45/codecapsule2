package main

import (
	"fmt"
	dlog "bitbucket.org/thespacecowboys45/dlogger"
)

func main() {
	fmt.Printf("Hello world\n");
	dlog.Always("Hello world\n")
}