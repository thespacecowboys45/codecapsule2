package main

import(
    "fmt"
    "net/http"
    "log"
    mypkg "./mypkg"
)


func mainHandler(w http.ResponseWriter, h *http.Request) {
    fmt.Printf("Got request\n")
    mypkg.Func1()

}

func main() {
    http.HandleFunc("/", mainHandler)
    log.Fatal(http.ListenAndServe(":8001", nil))
    fmt.Printf("listening")
}
