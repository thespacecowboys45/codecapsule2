package main

import(
    "log"
    "time"
    "strings"
    dlog "bitbucket.org/thespacecowboys45/dlogger"
    "gopkg.in/alecthomas/kingpin.v2"
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"

)



var (
    logLevel = kingpin.Flag("loglevel", "Set the log output level.  Default 'debug'.").Default("debug").Envar("LOG_LEVEL").String()
    host = kingpin.Flag("host", "Specify the database host.  Example: ln39bb.homedepot.com:27017").Default("ln39bb.homedepot.com:27017").Envar("HOST").String()
    database = kingpin.Flag("database", "Specify the database.  Example: davb_test").Default("davb_test").Envar("DATABASE").String()
    collection = kingpin.Flag("collection", "Specify the collection.  Example: fruits").Default("fruits").Envar("COLLECTION").String()
    username = kingpin.Flag("username", "Specify the username to connect with.").Default("").Envar("USERNAME").String()
    password = kingpin.Flag("password", "Specify the password to connect with.").Default("").Envar("PASSWORD").String()
)


type Fruit struct {
    Id bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
    Name string `json:"name"`
    Color string `json:"color"`
}


type Waveform struct {
    Id                      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Addr                    string
	Type                    string
	Name                    string
	Period                  int
	Rate                    int
	SimRate                 int
	InitialMetricValue      int
	CurrentMetricValue      int
	CurrentMetricValueIndex int
	MV1                     int
	MV2                     int
	Modulated               int
	Pattern                 []int
	Active                  int
}


func printFlags() {
    dlog.Always("logLevel:%s", *logLevel)
    dlog.Always("host:%s", *host)
    dlog.Always("database:%s", *database)
    dlog.Always("collection:%s", *collection)
    dlog.Always("username:%s", *username)
    dlog.Always("password:%s", *password)
}

func configureDlogger() {
    dlog.SetLevel(*logLevel)
}



func connect() *mgo.Session {

    dlog.Always("connect()")

    session, err := mgo.Dial(*host)
	if err != nil {
		dlog.Error("ERR: %s", err)
		return nil
	}

	if session == nil {
		dlog.Error("Error connecting to mongo")
		return nil
	}
	//defer session.Close()

	dlog.Always("connected!")
	return session
}

func connectAuthorized() *mgo.Session {
    dlog.Always("connectAuthorized()")

    hosts := strings.Split(*host, ",")

    info := &mgo.DialInfo{
        Addrs: hosts,
        Database: *database,
        Timeout: time.Second * 30,
        Username: *username,
        Password: *password,
    }
    session, err := mgo.DialWithInfo(info)
    if err != nil {
        dlog.Error("ERR: %s", err)
        return nil
    }

    if session == nil {
        dlog.Error("Error connecting to mongo")
        return nil
    }
    dlog.Always("connected!")
    return session
}



func disconnect(s *mgo.Session) {

    dlog.Always("disconnect()")
    s.Close()
    dlog.Always("closed!")
}

type Persons struct {
    Name string
    Phone string
}



func create(session *mgo.Session) error {

    dlog.Always("create()")

    //s := session.Copy()
    //defer s.Close()

    f := &Fruit {
        Id: bson.NewObjectId(),
        Name: "apple",
        Color: "juicy2",
    }

    dlog.Debug("Inserting fruit=%v", f)
    c := session.DB(*database).C(*collection)
    err := c.Insert(f)
    if err != nil {
        dlog.Error("create-err=%s", err)
        return err
    }

    dlog.Always("inserted!")

    return nil
}

func lookup(session *mgo.Session, collection string) []*Waveform {

    var results []*Waveform
    c := session.DB(*database).C(collection)
    err := c.Find(bson.M{}).All(&results)
    if err != nil {
        log.Fatal(err)
    }

    dlog.Debug("results:%v", results)
    for _, f := range results {
        dlog.Debug("RESULT:%v", f)
        dlog.Debug("Id:%s hex:%s", f.Id, f.Id.Hex())
    }
    return results
}

func destroy(session *mgo.Session) {
    // destroy one of our fruit!
    c := session.DB(*database).C(*collection)

    oneFruit := &Fruit{}
    err := c.Find(bson.M{}).One(&oneFruit)
    if err != nil {
        log.Fatal(err)
    }

    dlog.Debug("oneFruit:%v", oneFruit)

    dlog.Debug("Id:%s", oneFruit.Id)

    err = c.Remove(bson.M{"_id": oneFruit.Id})
    if err != nil {
        dlog.Error("Error removing fruit: %s", err)
    }
}


func update(find string, replace string, session *mgo.Session) {
    dlog.Always("update() change all %s to %s", find, replace)

    var results []*Fruit
    c := session.DB(*database).C(*collection)
    err := c.Find(bson.M{"name": find}).All(&results)
    if err != nil {
        dlog.Error("Cannot find any fruit matching name=%s", find)
    }

    for _, f := range results {
        dlog.Debug("[%s] Found Fruit %s %s", f.Id, f.Name, f.Color)
        f.Name = replace
        err := c.Update(bson.M{"_id": f.Id}, f)
        if err != nil {
            dlog.Error("error updating id:%s", f.Id)
        }
    }

    dlog.Always("update complete!")
}

func updateOne(wf *Waveform, session *mgo.Session) {
    dlog.Debug("updateOne: %v", wf)
    dlog.Debug("updateOne update id=%s", wf.Id.Hex())

    hexId := wf.Id.Hex()
    dlog.Debug("updateOne hexId=%s", hexId)

    var results *Waveform


    bsonId := bson.ObjectIdHex(hexId)

    dlog.Debug("updateOne translate back to: %s", bsonId)

    c := session.DB(*database).C(*collection)
    err := c.Find(bson.M{"_id": bsonId}).One(&results)
    if err != nil {
        dlog.Error("updateOne Cannot find any result err=%s", err)
    } else {
        dlog.Debug("updateOne Found record: %v ", results )

        results.Type = "random"
        err := c.Update(bson.M{"_id": bsonId}, results)
        if err != nil {
            dlog.Error("updateOne error updating: %s", err)
        } else {
            dlog.Debug("updateOne updated!")
        }
    }
}


func main() {
    kingpin.Parse()
    configureDlogger()
    printFlags()

    //s := connect()
    s := connectAuthorized()
    if s != nil {
        //create(s)

        wfs := lookup(s, *collection)

        //update("apple", "banana", s)

        updateOne(wfs[0], s)

        //destroy(s)

        disconnect(s)

    } else {
        dlog.Error("Could not connect")
    }

}