package main

import (
    //"fmt"
    dlog "bitbucket.org/thespacecowboys45/dlogger"
    "time"
    )

var c chan int
var ticker *time.Ticker


func startTicker() {

/*    go func() {
        ticker = time.NewTicker(time.Second * 1)
        for _ = range ticker.C {
            dlog.Always("Tick")
        }
        dlog.Always("go func1() [ exit ]")
    }()
*/

ticker = time.NewTicker(time.Second * 1)
go func() {
    for {
        select {
            case msg := <-c:
                dlog.Always("Got message:%d", msg)
                ticker.Stop()
                return
            case t := <-ticker.C:
                dlog.Always("ticket at %v", t)
        }
    }
    dlog.Always("go func2() [ exit ]")
}()

    dlog.Always("startTicker() [ exit ]")
}

func killTicker() {
    c<-1
}

func main() {
    dlog.Always("main()")

    c = make(chan int)

    startTicker()


    dlog.Always("Waiting")
    time.Sleep(time.Second * 2)
    dlog.Always("kill")
    killTicker()
    time.Sleep(time.Second * 2)
    dlog.Always("done waiting")
}