package main

import(
    "encoding/json"
    "fmt"
    "log"
)

type Bird struct {
    Color string `json:"color"`
    Name string `json:"name"`
}

type HostStruct struct {
    Href string `json:"href"`
}

type HostRolesStruct struct {
    ClusterName string `json:"cluster_name"`
    ComponentName string `json:"component_name"`
    HostName string `json:"host_name"`
    State string `json:"state"`
}

type Item struct {
    Href string `json:"href"`
    HostRoles HostRolesStruct `json:"HostRoles"`
    Host HostStruct `json:"host"`
}

type AmbariResponse struct {
    Href string `json:"href"`
    Items []Item `json:"items"`
}

type TestMarshallStruct struct {
    Field1 string
    Field2 string `json:"field_two"`
}

func main() {

    var data = []byte(`{ "color":"blue", "name":"bluebird"}`)
    log.Printf("[main][entry]")

    var bird Bird
    err := json.Unmarshal(data, &bird)
    if err != nil {
        log.Fatal("Cannot parse: ", err)
    }
    log.Printf("parsed: %v\n", bird)

    //var test1 = []byte(`{"href" : "http://atl-staging-ambari.io.hhost_components?fields=HostRoles/state"}`)
    /*
    var test1 = []byte(`{"href" : "http://atl-staging-ambari.io.hhost_components?fields=HostRoles/state",
        "items" : [
            {
                "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com/host_components/KAFKA_BROKER"
            }
        ]}`)
        */

 var test1 = []byte(`
{
  "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com/host_components?fields=HostRoles/state",
  "items" : [
    {
      "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com/host_components/KAFKA_BROKER",
      "HostRoles" : {
        "cluster_name" : "PR_ATL_STAGING",
        "component_name" : "KAFKA_BROKER",
        "host_name" : "msdis0202.homedepot.com",
        "state" : "STARTED"
      },
      "host" : {
        "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com"
      }
    },
    {
      "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com/host_components/ZOOKEEPER_SERVER",
      "HostRoles" : {
        "cluster_name" : "PR_ATL_STAGING",
        "component_name" : "ZOOKEEPER_SERVER",
        "host_name" : "msdis0202.homedepot.com",
        "state" : "STARTED"
      },
      "host" : {
        "href" : "http://atl-staging-ambari.io.homedepot.com:8081/api/v1/clusters/PR_ATL_STAGING/hosts/msdis0202.homedepot.com"
      }
    }
  ]
}`)

    var output AmbariResponse
    err = json.Unmarshal(test1, &output)
    if err != nil {
        log.Fatal("cannot parse response: %s", err)
    }
    log.Printf("parsed response: %v\n", output)


    // Test to see about marshalling a struct into a JSON string
    var tms = &TestMarshallStruct{
        Field1 : "fart",
        Field2 : "fix",
    }

    fmt.Printf("tms: %v\n", tms)
    b, err := json.Marshal(tms)
    if err != nil {
        log.Fatal("Cannot marshal json: %s\n", err)
    }
    fmt.Printf("json bytes: %v\n", b)
    fmt.Printf("json: %s\n", string(b))
}