package testpkg

import "fmt"

type Astruct struct {
	var1 string
	var2 string
	var3 string
}

func printAddr(s *Astruct) {
	fmt.Printf("printAddr: s=%v &s=%v *s=%v\n", s, &s, *s)
}

// Still, this needs to be called on an instantiation of an object
// and so the memory is already allocated and set when Getstruct()
// is called on the initial instantiation
func (a Astruct) ShowMe() {
	fmt.Printf("ShowMe: a=%v &a=%v\n", a, &a)
}

func (a Astruct) Setvals1(val1 string, val2 string) {
	fmt.Printf("Setvals1()\n")

	// no effect
	a.var1 = "me"
	a.var2 = val2
	a.var3 = "ping"

	fmt.Printf("in Setvals1 type=%T, a=%v, &a=%v\n", a, a, &a)

	printAddr(&a)

	// cannot do this
}

func (a Astruct) Setvals2(s *Astruct, val1 string, val2 string) {
	s.var1 = val1
	s.var2 = val2
}

func Setvals3(s *Astruct, val1 string, val2 string) {
	s.var1 = val1
	s.var2 = val2
}

func (a *Astruct) Setvals4(val1 string, val2 string) {

	a.var1 = val1
	a.var2 = val2

	printAddr(a)
}

func Getstruct(val1 string, val2 string) *Astruct {
	as := &Astruct{
		var1: val1,
		var2: val2,
	}

	as.var3 = "thisisset"
	return as
}