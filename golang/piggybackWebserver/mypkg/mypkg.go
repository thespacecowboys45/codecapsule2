package mypkg

import(
    "fmt"
    "net/http"
)

func Func1() {
    fmt.Printf("mypkg - func1()\n")
}

func mypkgHandler(w http.ResponseWriter, h *http.Request) {
    fmt.Printf("mypkgHandler()\n")
}

func init() {

    http.HandleFunc("/mypkg", mypkgHandler)

    fmt.Printf("mypkg.go - init()\n")
}