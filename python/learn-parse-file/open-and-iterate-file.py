#!/usr/bin/python

# One easy way to create a block comment is to make a text variable with your documentation
DOCUMENTATION = '''
---
Iterate over a file and print each line to the screen
'''

# the name of our file to read
filename = "data.csv"

print("filename="+filename)

# open the file for reading
f = open(filename, "r")

# iterate over each line in the file
for line in f:
    # print out the line in the file
    print(line)