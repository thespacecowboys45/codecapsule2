package mypackage

import(
	"fmt"
)

// To use a packaged function it must be exported
// Export by using an upper case character as the first letter
func Packagefunc() {
	fmt.Printf("packagefunc()\n")
}